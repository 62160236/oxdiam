/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.krorawitt.oxdiam;

/**
 *
 * @author WINDOWS 10
 */
import java.util.Scanner;
public class Game {
    Player playerX;
    Player playerO;
    Player turn;
    Board board;
    int row, col;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        board = new Board(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        board.showTable();
    }

    public void input() {
        //Normal flow
        while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (board.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
        }
    }

    public void showTurn() {
        System.out.println(board.getcurrentPlayer().getName() + " turn");
    }
    public void newGame(){
        board = new Board(playerX, playerO);
        System.out.println("Start a New Game");
    }
    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            board.checkWin();
            if(board.isFinish()){
                this.showTable();
                if(board.getWinner()==null){
                    System.out.println("Draw!!!");
                }else{
                    System.out.println(board.getWinner().getName() + " Win!!");
                    System.out.println("Bye bye");
                    break;
                }
            }
            board.switchPlayer();
        }
    }
}
